# git 操作指南

## 克隆远程仓库到本地

> ```
> git clone https://gitee.com/canvnac/git-stu.git
> ```

--------------------------------

## 拉取远程 dev 分支

> ```
> git checkout -b dev origin/dev
> ```


--------------------------------

## 日常任务开发
1. 建立本地开发分支



> ```
> // 切换到开发主分支
> git switch dev
> 
> // 建立本地独立开发分支
> git switch -c dev1209
> 
> // 进行任务开发并提交
> // ...
> git commit -m "完成任务....."
> ```

2. 合入主开发分支，并提交远程仓库



> ```
> // 切换到开发主分支
> git switch dev
> 
> // 合并本地独立开发分支内容到主开发分支
> git merge dev1209
> 
> // 提交远程仓库
> git pull
> git push origin dev
> 
> // 删除本地独立开发分支
> git branch -d dev1209
> ```

3. 开发到一半，任务取消了，删除本地独立开发分支



> ```
> // 切换到开发主分支
> git switch dev
> // 删除本地独立开发分支
> git branch -d dev1209
> // 如果通过以上命令销毁失败，进行强制销毁
> git branch -D dev1209
> ```

--------------------------------
## Bug修复
1. 当前有任务开发到一半，且不能提交，先进行缓存，假定 dev1208 分支



> ```
> // 查看工作区有哪些被修改了(如果有文件没被git管理到，先执行 git add .)
> git status
> 
> // 保存现场
> git stash
> ```

2. 开始修复工作



> ```
> // 切换到要修复的分支，假定 dev 分支
> git checkout dev
> git pull
> 
> // 创建临时分支
> git checkout -b bug1209
> 
> // 在临时分支上进行修复工作
> // ...
> git commit -m "fix bug1209"
> 
> // 修复完成后，切换到 dev 分支，并完成合并，最后删除 bug1209 分支
> git switch dev
> git merge --no-ff -m "merged bug fix bug1209" bug1209
> git branch -d bug1209
> 
> // 推送 dev 到远程仓库
> git pull
> git push origin dev
> ```

3. 恢复现场，继续之前的任务

> ```
> // 回到原来分支，假定 dev1208 分支
> git switch dev1208
> 
> // 查看现场列表
> git stash list
> ```

- 第一种恢复现场方式

> ```
> // 恢复现场，同时删除 stash 内容
> git stash pop
> ```

- 第二种恢复现场方式，有多个现场，恢复其中一个


> ```
> // 恢复某个现场
> git stash apply stash@{0}
> 
> // 删除该现场对应的 stash 内容
> git stash drop stash@{0}
> ```

--------------------------------
## 打版本标签
1. 正常打版本标签



> ```
> // 假定要在 dev 分支上打版本标签，切换分支
> git checkout dev
> 
> // 打标签
> git tag v1.0.0.20211202
> 
> // 查看标签记录
> git tag
> 
> // 推送标签到远程仓库
> git push origin v1.0.0.20211202
> ```

2. 补打标签，例如前天的标签忘记打了，现在补打



> ```
> // 假定要在 dev 分支上打版本标签，切换分支
> git checkout dev
> 
> // 查看历史提交的 commitid
> git log --pretty=oneline --abbrev-commit
> 
> // 打标签，指令中 f52c6aa为某个提交的commitid
> git tag v1.0.0.20211202 f52c6aa
> 
> // 如果想给标签一些说明，可以用以下指令
> git tag -a v1.0.0.20211202 -m "标签说明" f52c6aa
> 
> // 查看标签详细信息
> git show v1.0.0.20211202
> ```


